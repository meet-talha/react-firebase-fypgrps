export interface IGroup {
  id: string;
  name: string;
  ownerId: string;

  isCurrentUserMember?: boolean;
}

export interface IUser {
  id: string;
  uid: string;
  name: string;
  rollNo: string;
  groupId: string | null;
}

export interface IGroupDictionary {
  [index: string]: IGroup;
}

export interface IUserDictionary {
  [index: string]: IUser;
}