import * as React from 'react';

import Group from './Group';

import { IGroupDictionary, IUserDictionary } from './types';

interface IGroupListProps {
  deleteGroup: (groupId: string) => void;
  groups: IGroupDictionary;
  leaveGroup: () => void;
  logout: () => void;
  me: string | null;
  members: IUserDictionary;
  isLoggedIn: boolean;
  isMemberOfAnyGroup: boolean;
  createGroup: (groupName: string) => void;
  joinGroup: (groupId: string) => void;
}

class GroupList extends React.Component<IGroupListProps, {}> {
  private groupNameInput: HTMLInputElement;

  constructor(props: IGroupListProps) {
    super(props);

    this.handleCreateGroupSubmit = this.handleCreateGroupSubmit.bind(this);
  }

  public handleCreateGroupSubmit(evt: React.SyntheticEvent<HTMLFormElement>) {
    evt.preventDefault();

    const { createGroup } = this.props;

    createGroup(this.groupNameInput.value);

    evt.currentTarget.reset();
  }

  public render() {
    const { handleCreateGroupSubmit } = this;
    const { groups, members, isMemberOfAnyGroup, me, deleteGroup, leaveGroup, joinGroup, logout } = this.props;

    const groupKeys = Object.keys(groups);

    const groupsWithMe = groupKeys.reduce( (accumulator: IGroupDictionary, groupId: string) => {
      const group = groups[groupId];

      if(me !== null) {
        group.isCurrentUserMember =
          typeof members[me].groupId !== 'undefined' &&
            members[me].groupId === groupId;
      } else {
        group.isCurrentUserMember = false;
      }

      accumulator[groupId] = group;

      return accumulator;
    }, {});

    groupKeys.sort((groupAId: string, groupBId: string) => {
      const groupA = groupsWithMe[groupAId];
      const groupB = groupsWithMe[groupBId];

      if(groupA && groupB) {
        if(groupA.isCurrentUserMember && groupB.isCurrentUserMember) {
          return 0;
        } else if(groupA.isCurrentUserMember) {
          return -1;
        } else {
          return 1;
        }
      } else {
        return 0;
      }
    });

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="d-flex justify-content-between mb-3">
              {
                !isMemberOfAnyGroup &&
                <form className="form-inline" onSubmit={handleCreateGroupSubmit}>
                  <div className="form-group mr-2">
                    <input
                      ref={(elem: HTMLInputElement): void => { this.groupNameInput = elem }}
                      required={true}
                      className="form-control"
                      placeholder="New Group Name"
                      name="name"
                    />
                  </div>
                  <button
                    type="submit"
                    className="list-group-add btn btn-secondary"
                  >
                    Create Group
                  </button>
                </form>
              }
              <button className="btn" type="button" onClick={logout}>Sign Out</button>
            </div>
            {
              !isMemberOfAnyGroup &&
                <div className="alert alert-info mb-3">
                  It appears that you are not a part of any group. Create your own or join an existing one.
                </div>
            }
            {
              groupKeys.length > 0 &&
                <div className="row">
                  {
                    groupKeys.map( (id: string): React.ReactElement<Group> => {
                      const group = groups[id];

                      return (
                        <Group
                          key={id}
                          groupId={id}
                          me={me}
                          isMemberOfAnyGroup={isMemberOfAnyGroup}
                          group={group}
                          members={members}
                          deleteGroup={deleteGroup}
                          leaveGroup={leaveGroup}
                          joinGroup={joinGroup}
                        />
                      );
                    })
                  }
                </div>
            }

            {
              groupKeys.length === 0 &&
                <div className="alert alert-info">
                  No groups so far. Make your own and ask your friends to join.
                </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default GroupList;