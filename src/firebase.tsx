import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyDHBgOC6Wzat_ngr-5TnoUGZlxcVksUhlI",
  authDomain: "fyp-spy-batch-2015.firebaseapp.com",
  databaseURL: "https://fyp-spy-batch-2015.firebaseio.com",
  messagingSenderId: "476331559157",
  projectId: "fyp-spy-batch-2015",
  storageBucket: ""
};

firebase.initializeApp(config);

export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;