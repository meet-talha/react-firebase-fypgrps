import * as React from 'react';

import './Header.css';

class Header extends React.Component {
	public render() {
		return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <a className="navbar-brand" href="javascript:void(0)">Final Year Project | Batch 2015 Groups</a>
        </div>
      </nav>
    );
	}
}

export default Header;