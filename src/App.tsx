import * as React from 'react';

import './App.css';

import GroupList from './GroupList';
import Header from './Header';

import { IGroupDictionary, IUserDictionary } from './types';

import firebase, { auth, provider } from './firebase';

interface IAppState {
  groups: IGroupDictionary;
  members: IUserDictionary;
  me: string | null;
  isLoading: boolean;
  recentlyLoggedInWithInvalidAccount: boolean;
}

class App extends React.Component<{}, IAppState> {

  public state: IAppState = {
    groups: {},
    isLoading: true,
    me: null,
    members: {},
    recentlyLoggedInWithInvalidAccount: false
  };

  private fbGroupsRef = firebase.database().ref('groups');
  private fbMembersRef = firebase.database().ref('members');

  
  constructor(props: {}) {
    super(props);

    this.createGroup = this.createGroup.bind(this);
    this.deleteGroup = this.deleteGroup.bind(this);
    this.leaveGroup = this.leaveGroup.bind(this);
    this.joinGroup = this.joinGroup.bind(this);
    this.login = this.login.bind(this);
    this.loginWithGuard = this.loginWithGuard.bind(this);
    this.logout = this.logout.bind(this);
  }

  public componentDidMount() {
    const { fbGroupsRef, fbMembersRef, loginWithGuard } = this;

    window.scrollTo(0, 0);

    fbGroupsRef.on('value', (snapshot) => {
      if(snapshot !== null) {
        const groups = snapshot.val() || {};
        
        this.setState({
          groups
        });
      }
    });

    fbMembersRef.on('value', (snapshot) => {
      if(snapshot !== null) {
        const members = snapshot.val() || {};
        
        this.setState({
          members
        });

        auth.onAuthStateChanged((user) => {
          if(user !== null) {
            loginWithGuard(user);
          }

          this.setState({
            isLoading: false
          });
        });
      }
    });
  }

  public login() {
    const { loginWithGuard } = this;

    auth.signInWithPopup(provider) 
      .then((result) => {
        const user = result.user;

        loginWithGuard(user);
      });
  }

  public logout() {
    auth.signOut()
      .then(() => {
        this.setState({
          me: null
        });
      });
  }

  public createGroup(groupName: string): void {
    const { fbGroupsRef, fbMembersRef } = this;

    const ownerId = this.state.me;

    if(ownerId !== null) {
      const newFbGroup = {
        name: groupName,
        ownerId
      };

      const fbGroupRef = fbGroupsRef.push(newFbGroup);
      const newGroupId = fbGroupRef.key as string;
      
      fbMembersRef.child(`${ownerId}/groupId`).set(newGroupId);
    }
  }

  public deleteGroup(groupId: string) {
    const { fbGroupsRef, fbMembersRef } = this;
    const { members } = this.state;

    const memberKeys = Object.keys(members);

    const groupMemberKeys = memberKeys.filter(
      (memberId: string) => 
        members[memberId].groupId &&
          members[memberId].groupId === groupId
    );

    groupMemberKeys.forEach( (memberId: string) => {
      fbMembersRef.child(`${memberId}/groupId`).set(null);
    });

    fbGroupsRef.child(groupId).set(null);
  }

  public joinGroup(groupId: string) {
    const { fbMembersRef } = this;
    const me = this.state.me;

    if(me !== null) {
      fbMembersRef.child(`${me}/groupId`).set(groupId);

      window.scrollTo(0, 0);
    }
  }

  public leaveGroup() {
    const { fbMembersRef } = this;
    const me = this.state.me;

    if(me !== null) {
      fbMembersRef.child(`${me}/groupId`).set(null);
    }
  }

  public render() {
    const { createGroup, login, deleteGroup, leaveGroup, joinGroup, logout } = this;
    const { groups, members, me, isLoading, recentlyLoggedInWithInvalidAccount } = this.state;

    const isLoggedIn: boolean = me !== null;
    const isMemberOfAnyGroup: boolean =
      me !== null &&
        typeof members[me] !== 'undefined' &&
        typeof members[me].groupId !== 'undefined';

    return (
      <React.Fragment>
        <Header />
        {
          isLoading &&
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="alert alert-info">
                    The list is loading. Please wait.
                  </div>
                </div>
              </div>
            </div>
        }
        {
          !isLoading &&
            <React.Fragment>
              {
                isLoggedIn &&
                  <GroupList
                    isMemberOfAnyGroup={isMemberOfAnyGroup}
                    isLoggedIn={isLoggedIn}
                    createGroup={createGroup}
                    deleteGroup={deleteGroup}
                    leaveGroup={leaveGroup}
                    joinGroup={joinGroup}
                    groups={groups}
                    logout={logout}
                    members={members}
                    me={me}
                  />
              }
              {
                !isLoggedIn && recentlyLoggedInWithInvalidAccount &&
                  <div className="container">
                    <div className="row">
                      <div className="col-md-12">
                        <div className="alert alert-danger mb-3">
                          You need to sign in with an university issued email.
                        </div>
                      </div>
                    </div>
                  </div>
              }
              {
                !isLoggedIn &&
                  <div className="container">
                    <div className="row">
                      <div className="col-md-12">
                        <div className="alert alert-info">
                          You need to sign in before you can view groups. 
                          <a className="ml-2" href="javascript:void(0)" onClick={login}>Sign In</a>
                        </div>
                      </div>
                    </div>
                  </div>
              }
            </React.Fragment>
        }
      </React.Fragment>
    );
  }

  private loginWithGuard(user: any) {
    const { fbMembersRef, logout } = this;
    const { members } = this.state;

    const memberKeys = Object.keys(members);

    const matchingMemberKeys = memberKeys.filter(
      (id: string): boolean => members[id].uid === user.uid
    );

    const isExistingMember: boolean = matchingMemberKeys.length === 1;
    const isNotExistingMember: boolean = matchingMemberKeys.length === 0;

    if(isExistingMember) {
      this.setState({
        me: matchingMemberKeys[0],
        recentlyLoggedInWithInvalidAccount: false
      });
    }
    else if(isNotExistingMember) {
      const lowercasedEmail = user.email.toLowerCase();
      const uniEmailRegex = /^(l1[0-5]\d{4})@lhr.nu.edu.pk$/;

      if(uniEmailRegex.test(lowercasedEmail)) {
        const rollNoMatches = lowercasedEmail.match(uniEmailRegex);
        const rollNo = rollNoMatches[1];

        const newFbUser = {
          groupId: null,
          name: user.displayName,
          rollNo,
          uid: user.uid,
        };

        const fbMemberRef = fbMembersRef.push(newFbUser);

        this.setState({
          me: fbMemberRef.key as string,
          recentlyLoggedInWithInvalidAccount: false
        });
      }
      else {
        this.setState({
          recentlyLoggedInWithInvalidAccount: true
        });

        logout();
      }
    }
  }
}

export default App;
