import * as React from 'react';

import { IGroup, IUserDictionary } from './types';

interface IGroupProps {
  deleteGroup: (groupId: string) => void;
  group: IGroup;
  groupId: string;
  isMemberOfAnyGroup: boolean;
  leaveGroup: () => void;
  me: string | null;
  members: IUserDictionary;
  joinGroup: (groupId: string) => void;
}

class Group extends React.Component<IGroupProps, {}> {
  constructor(props: IGroupProps) {
    super(props);

    this.handleDeleteGroup = this.handleDeleteGroup.bind(this);
    this.handleJoinGroup = this.handleJoinGroup.bind(this);
  }

  public handleDeleteGroup() {
    const { groupId, deleteGroup } = this.props;

    deleteGroup(groupId);
  }

  public handleJoinGroup() {
    const { groupId, joinGroup } = this.props;

    joinGroup(groupId);
  }

  public render() {
    const { handleDeleteGroup, handleJoinGroup } = this;

    const { me, members, isMemberOfAnyGroup, groupId, leaveGroup } = this.props;
    const { name, ownerId } = this.props.group;
    const memberKeys = Object.keys(members);

    const groupMemberKeys = memberKeys.filter(
      (memberId: string) => 
        members[memberId].groupId &&
          members[memberId].groupId === groupId
    );

    const isOwnerOfThisGroup = ownerId === me;

    const isMemberOfThisGroup = 
      me !== null && groupMemberKeys.indexOf(me) > -1;

    return (
      <div className="col-md-6 col-lg-6 col-xl-4 mb-3">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">
              { name }
              {
                isMemberOfThisGroup &&
                  <span className="badge badge-primary ml-2">
                    {
                      isOwnerOfThisGroup ? 'Owner' : 'Member'
                    }
                  </span>
              }
            </h5>
            <h6>Members</h6>
            <div className="mb-3">
              {
                groupMemberKeys.length > 0 &&
                  <ul className="list-group">
                    {
                      groupMemberKeys.map( (id: string): React.ReactElement<HTMLLIElement> => {
                        const member = members[id];

                        return (
                          <li
                            key={id}
                            className="list-group-item"
                          >
                            <small className="badge badge-secondary mr-2">{ member.rollNo }</small>
                            { member.name }
                          </li>
                        );
                      })
                    }
                  </ul>
              }
              {
                groupMemberKeys.length === 0 &&
                  <div className="alert alert-info">
                    No members were found in this group.
                  </div>
              }
            </div>
            {
              !isMemberOfAnyGroup &&
                <a href="javascript:void(0)" className="btn btn-primary" onClick={handleJoinGroup}>Join Group</a>
            }
            {
              isMemberOfAnyGroup && isOwnerOfThisGroup &&
                <a href="javascript:void(0)" className="btn btn-primary" onClick={handleDeleteGroup}>Delete Group</a>
            }
            {
              isMemberOfAnyGroup && !isOwnerOfThisGroup && isMemberOfThisGroup &&
                <a href="javascript:void(0)" className="btn btn-primary" onClick={leaveGroup}>Leave Group</a>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default Group;